var APP_DATA = {
  "scenes": [
    {
      "id": "0-entrance-p102",
      "name": "Entrance P102",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.10805737772647106,
        "pitch": -0.29031072242589673,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -0.38129859168569524,
          "pitch": -0.0501251899604771,
          "rotation": 6.283185307179586,
          "target": "1-main-hall"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-main-hall",
      "name": "Main Hall",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.8304824311096084,
        "pitch": -0.06864030813054356,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -1.5011905959848484,
          "pitch": -0.005275428341743549,
          "rotation": 0,
          "target": "0-entrance-p102"
        },
        {
          "yaw": 0.3568768784182126,
          "pitch": 0.02791260468994672,
          "rotation": 0,
          "target": "2-office"
        },
        {
          "yaw": 2.6216182749598334,
          "pitch": 0.019758519664163643,
          "rotation": 3.141592653589793,
          "target": "3-main-hall-workspace"
        },
        {
          "yaw": 1.6921531194248853,
          "pitch": 0.08548696076574025,
          "rotation": 0,
          "target": "4-conference-area"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.8343487497723316,
          "pitch": 0.05752775847472513,
          "title": "Fab Kitchen",
          "text": ""
        }
      ]
    },
    {
      "id": "2-office",
      "name": "Office",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.4111795310550459,
        "pitch": 0.17757382869869964,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -2.799063323317922,
          "pitch": 0.06609946278462431,
          "rotation": 0,
          "target": "1-main-hall"
        },
        {
          "yaw": 2.392630999445122,
          "pitch": 0.04657078354322053,
          "rotation": 0,
          "target": "4-conference-area"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.2625141599630645,
          "pitch": 0.1317953022117102,
          "title": "Instructor",
          "text": "Oscar"
        }
      ]
    },
    {
      "id": "3-main-hall-workspace",
      "name": "Main Hall Workspace",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.2612429869099593,
        "pitch": -0.072148659233509,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -0.14525800097511876,
          "pitch": 0.015941508959606665,
          "rotation": 5.497787143782138,
          "target": "0-entrance-p102"
        },
        {
          "yaw": 0.15169628392481016,
          "pitch": 0.20152630076402822,
          "rotation": 3.141592653589793,
          "target": "1-main-hall"
        },
        {
          "yaw": 0.7523469146025583,
          "pitch": 0.03291326008652895,
          "rotation": 0,
          "target": "2-office"
        },
        {
          "yaw": 2.341564056918088,
          "pitch": 0.05154933482461743,
          "rotation": 7.0685834705770345,
          "target": "4-conference-area"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.9851457151031671,
          "pitch": 0.11391926247270234,
          "title": "Lunch Table",
          "text": ""
        },
        {
          "yaw": 2.7225662034835594,
          "pitch": 0.15064519612038296,
          "title": "Conference Area",
          "text": "Main lectures"
        }
      ]
    },
    {
      "id": "4-conference-area",
      "name": "Conference Area",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.332192709999891,
        "pitch": -0.01377160665929722,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 0.6361046366813898,
          "pitch": -0.9915800771773942,
          "rotation": 1.5707963267948966,
          "target": "5-fab-textiles"
        },
        {
          "yaw": -0.6218485397207179,
          "pitch": 0.07263056558618786,
          "rotation": 0,
          "target": "3-main-hall-workspace"
        },
        {
          "yaw": -0.15983435850229455,
          "pitch": 0.024036089893922252,
          "rotation": 0,
          "target": "1-main-hall"
        },
        {
          "yaw": 3.1222900017628064,
          "pitch": 0.1942024086857863,
          "rotation": 0,
          "target": "6-main-hall-kuka"
        },
        {
          "yaw": -2.9808713580789554,
          "pitch": 0.05169597933708836,
          "rotation": 1.5707963267948966,
          "target": "6-main-hall-kuka"
        },
        {
          "yaw": 2.9098881956738083,
          "pitch": 0.05217697008199451,
          "rotation": 4.71238898038469,
          "target": "8-fab-academy-room"
        },
        {
          "yaw": 1.9809743492839917,
          "pitch": 0.0623406317186852,
          "rotation": 0,
          "target": "10-electronics-room"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.446949040031976,
          "pitch": 0.12136530366059972,
          "title": "3D Printing room",
          "text": ""
        },
        {
          "yaw": -1.9513510336306918,
          "pitch": -0.2602359659835507,
          "title": "BCN MAP",
          "text": "Digitally fabricated"
        }
      ]
    },
    {
      "id": "5-fab-textiles",
      "name": "Fab Textiles",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.965457948964481,
        "pitch": -0.05083780345346156,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 2.191354422505455,
          "pitch": 0.129266643662465,
          "rotation": 5.497787143782138,
          "target": "4-conference-area"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "6-main-hall-kuka",
      "name": "Main Hall Kuka",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.10587243853263573,
        "pitch": 0.0690147913999315,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 0.5674033077442022,
          "pitch": 0.13503605478429215,
          "rotation": 6.283185307179586,
          "target": "8-fab-academy-room"
        },
        {
          "yaw": -0.7761176058419164,
          "pitch": 0.12071934974219012,
          "rotation": 0,
          "target": "9-fab-academy-room-2"
        },
        {
          "yaw": -1.654514417351379,
          "pitch": 0.16932566257141346,
          "rotation": 0,
          "target": "4-conference-area"
        },
        {
          "yaw": -1.442069976826314,
          "pitch": -0.465948714617328,
          "rotation": 7.853981633974483,
          "target": "5-fab-textiles"
        },
        {
          "yaw": 2.8338327718385656,
          "pitch": 0.02920360472838368,
          "rotation": 0,
          "target": "7-kuka-workshop"
        },
        {
          "yaw": 1.7234413264473467,
          "pitch": 0.03003239493988552,
          "rotation": 1.5707963267948966,
          "target": "11-wood-workshop"
        },
        {
          "yaw": 1.2098814000953553,
          "pitch": 0.03514188204975355,
          "rotation": 4.71238898038469,
          "target": "12-lasercutter-workshop"
        },
        {
          "yaw": 1.4744313199244399,
          "pitch": 0.03129509199337832,
          "rotation": 0,
          "target": "13-backyard"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.166249329474396,
          "pitch": 0.1950711731258732,
          "title": "Students Projects",
          "text": ""
        },
        {
          "yaw": -2.5168599949544586,
          "pitch": 0.10840027726999324,
          "title": "PugMill",
          "text": ""
        },
        {
          "yaw": -0.13556874662759455,
          "pitch": 0.28757552338769443,
          "title": "PopUp Lab",
          "text": ""
        }
      ]
    },
    {
      "id": "7-kuka-workshop",
      "name": "Kuka Workshop",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -2.771317125611949,
        "pitch": -0.06261546842327625,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 1.4957027193974488,
          "pitch": 0.13089504657822104,
          "rotation": 0,
          "target": "6-main-hall-kuka"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -2.7022803863633307,
          "pitch": 0.09018951328538805,
          "title": "Title",
          "text": "Text"
        },
        {
          "yaw": -0.7595019422141434,
          "pitch": 0.7147848393502478,
          "title": "Turntable",
          "text": "7 axis"
        }
      ]
    },
    {
      "id": "8-fab-academy-room",
      "name": "Fab Academy Room",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.11400443824824436,
        "pitch": -0.09222346457525532,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 1.4168377964044634,
          "pitch": 0.16682506858496104,
          "rotation": 3.141592653589793,
          "target": "9-fab-academy-room-2"
        },
        {
          "yaw": 0.9457658785825167,
          "pitch": 0.0676575052815025,
          "rotation": 0.7853981633974483,
          "target": "10-electronics-room"
        },
        {
          "yaw": 0.5190301433408564,
          "pitch": 0.04689542077321107,
          "rotation": 5.497787143782138,
          "target": "6-main-hall-kuka"
        },
        {
          "yaw": -0.054113147907989045,
          "pitch": 0.059005094468572494,
          "rotation": 0,
          "target": "11-wood-workshop"
        },
        {
          "yaw": -0.9664910497544561,
          "pitch": 0.012561982322790044,
          "rotation": 5.497787143782138,
          "target": "12-lasercutter-workshop"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.0992407298908713,
          "pitch": 0.424010599744971,
          "title": "Main ClassRoom",
          "text": "FabAcademy happens here"
        },
        {
          "yaw": -1.5442333162935498,
          "pitch": 0.04823176796650941,
          "title": "Tools-materials",
          "text": "rack"
        },
        {
          "yaw": -2.2724642783982496,
          "pitch": 0.22851757272896833,
          "title": "VinylCutters",
          "text": "Cameo-RolandGX24"
        },
        {
          "yaw": -0.7517423891869512,
          "pitch": -0.5294700647635437,
          "title": "FabLab network",
          "text": "World Map"
        }
      ]
    },
    {
      "id": "9-fab-academy-room-2",
      "name": "Fab Academy Room 2",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.05223359451439791,
        "pitch": 0.08326121560802946,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -0.2647564932886315,
          "pitch": 0.11075871336616316,
          "rotation": 0,
          "target": "8-fab-academy-room"
        },
        {
          "yaw": 0.9847939885791952,
          "pitch": 0.04851338056028354,
          "rotation": 0,
          "target": "11-wood-workshop"
        },
        {
          "yaw": 1.6073754464963779,
          "pitch": 0.06190795079132094,
          "rotation": 0,
          "target": "6-main-hall-kuka"
        },
        {
          "yaw": 2.2236706364462737,
          "pitch": 0.09879604192221336,
          "rotation": 1.5707963267948966,
          "target": "10-electronics-room"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.013390333615127759,
          "pitch": 0.6025369812130208,
          "title": "Molding and Casting",
          "text": "Ready for the assignment"
        }
      ]
    },
    {
      "id": "10-electronics-room",
      "name": "Electronics Room",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 0.08581388383685251,
        "pitch": 0.03881750347045809,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": 0.8576383767705966,
          "pitch": 0.0566717484555852,
          "rotation": 4.71238898038469,
          "target": "9-fab-academy-room-2"
        },
        {
          "yaw": 1.5293196817034902,
          "pitch": 0.045704434229211444,
          "rotation": 0,
          "target": "4-conference-area"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.2820576683794265,
          "pitch": 0.11259433306846489,
          "title": "Precision CNC",
          "text": "SRM-20"
        },
        {
          "yaw": -0.4692475170028274,
          "pitch": 0.11782551339957337,
          "title": "Precision CNC",
          "text": "MDX-20"
        },
        {
          "yaw": -3.0871663443888675,
          "pitch": -0.08152062036513996,
          "title": "Electronic Components",
          "text": "FabLab Inventory"
        }
      ]
    },
    {
      "id": "11-wood-workshop",
      "name": "Wood Workshop",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.663879837240648,
        "pitch": -0.010034297585214347,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -2.063977970645219,
          "pitch": 0.03041863056178329,
          "rotation": 4.71238898038469,
          "target": "7-kuka-workshop"
        },
        {
          "yaw": -1.7303917142837442,
          "pitch": 0.17709961959114828,
          "rotation": 3.141592653589793,
          "target": "6-main-hall-kuka"
        },
        {
          "yaw": -1.366508310030909,
          "pitch": 0.045855177165366356,
          "rotation": 1.5707963267948966,
          "target": "8-fab-academy-room"
        },
        {
          "yaw": -1.7301680916027138,
          "pitch": 0.03230073864173022,
          "rotation": 0,
          "target": "4-conference-area"
        },
        {
          "yaw": -0.848459243622905,
          "pitch": 0.0407600938197028,
          "rotation": 7.853981633974483,
          "target": "12-lasercutter-workshop"
        },
        {
          "yaw": 1.0643583320499985,
          "pitch": 0.0024083175238587273,
          "rotation": 0,
          "target": "13-backyard"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 3.038631429087131,
          "pitch": 0.1868001527358416,
          "title": "Big CNC",
          "text": "RAPTOR X-SL"
        },
        {
          "yaw": 2.228101708152174,
          "pitch": -0.08533286180228927,
          "title": "Big CNC",
          "text": "Precix"
        },
        {
          "yaw": 1.723229673363761,
          "pitch": 0.08515545776334932,
          "title": "Material",
          "text": "Storage"
        },
        {
          "yaw": 0.3419355199613445,
          "pitch": 0.10878329438228462,
          "title": "Workbench",
          "text": ""
        }
      ]
    },
    {
      "id": "12-lasercutter-workshop",
      "name": "LaserCutter Workshop",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.403836135038132,
        "pitch": 0.07486083816190003,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -0.8584491803424648,
          "pitch": 0.09517881418979002,
          "rotation": 5.497787143782138,
          "target": "11-wood-workshop"
        },
        {
          "yaw": -0.06489922707065432,
          "pitch": 0.04418119003653942,
          "rotation": 0,
          "target": "8-fab-academy-room"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.992102201536241,
          "pitch": 0.26104054943319,
          "title": "Lasercutter",
          "text": "Multicam 300w"
        },
        {
          "yaw": 2.4982671102376433,
          "pitch": -0.015139241343918641,
          "title": "Lasercutter",
          "text": "Speedy400"
        },
        {
          "yaw": 1.5568745226517962,
          "pitch": 0.032196983008633495,
          "title": "Lasercutter",
          "text": "Speedy100"
        },
        {
          "yaw": -2.6676795052735383,
          "pitch": -0.02889080558334456,
          "title": "Welding area",
          "text": "Metalworkshop"
        },
        {
          "yaw": -2.0394342421155525,
          "pitch": -0.004439261931427296,
          "title": "Plastic Recycling",
          "text": "Precious plastics"
        },
        {
          "yaw": -2.272392795634973,
          "pitch": 0.42232771557050874,
          "title": "Romi Project",
          "text": ""
        },
        {
          "yaw": 0.675170238348521,
          "pitch": -0.15299461123740876,
          "title": "Storage",
          "text": ""
        },
        {
          "yaw": 0.9877087848477686,
          "pitch": 0.19005387958263853,
          "title": "Vacuum table",
          "text": ""
        },
        {
          "yaw": -1.3188185239898154,
          "pitch": 0.2564769604041892,
          "title": "HandTools",
          "text": ""
        }
      ]
    },
    {
      "id": "13-backyard",
      "name": "Backyard",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.7349888659601227,
        "pitch": -0.006042058495859592,
        "fov": 1.3861769216464677
      },
      "linkHotspots": [
        {
          "yaw": -0.47282770022793663,
          "pitch": -0.0170971947354559,
          "rotation": 1.5707963267948966,
          "target": "11-wood-workshop"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.9061502562402328,
          "pitch": 0.07761776125118658,
          "title": "Bicycle rack",
          "text": ""
        },
        {
          "yaw": -1.0530504534127125,
          "pitch": -0.21641040937474898,
          "title": "LongTerm storage",
          "text": ""
        },
        {
          "yaw": 1.4995143926838512,
          "pitch": 0.11090111238080702,
          "title": "Dirty things workspace",
          "text": ""
        }
      ]
    }
  ],
  "name": "FabLab Barcelona P102",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": true,
    "viewControlButtons": true
  }
};
